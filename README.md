# [javadev.ru](https://javadev.ru) source codes

<br/>

### Run javadev.ru on localhost

    # vi /etc/systemd/system/javadev.ru.service

Insert code from javadev.ru.service

    # systemctl enable javadev.ru.service
    # systemctl start javadev.ru.service
    # systemctl status javadev.ru.service

http://localhost:4032
