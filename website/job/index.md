---
layout: page
title: Предложить работу
description: Предложить работу
keywords: Предложить работу
permalink: /job/
---

# Предложить работу!

Если вдруг на проект, понадобится человек с моим опытом, напишите на адрес эл.почты.

Могу Админить Linux, Oracle DataBase, Weblogic, WilfFly (Jboss)  
Программировать на: Java EE, PL/SQL, Node.js

Можно удаленно, можно локально. Могу поехать в командировку.

<br/>

Для контактов:<br/>
<img src="/img/a3333333mail.gif" alt="Marley" border="0" />
