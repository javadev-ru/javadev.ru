---
layout: page
title: OpenSource проекты на Java
description: OpenSource проекты на Java
keywords: OpenSource проекты на Java
permalink: /src/
---

# OpenSource проекты на Java

<br/>

## Java

https://github.com/alkacon/opencms-core  
https://github.com/jirkapinkas/example-eshop-vaadin  
https://github.com/jirkapinkas/jsfeshop

<br/>

## Spring Framework

### [Udemy] Building An E-Commerce Store Using Java Spring Framework [ENG, 2016]

https://github.com/webmakaka/Building-an-e-commerce-store-using-java-spring-framework

<br/>

### [Книга] Spring MVC. Alex Bretet - Spring MVC Cookbook - 2016

Исходные коды:  
https://github.com/alex-bretet/cloudstreetmarket.com

<br/>

### Кислин Григорий

https://github.com/JavaOPs/topjava

<br/>

## Oracle ADF

http://oracle-adf.com/src/
