---
layout: page
title: Подключение сервера приложений Jboss 7.X к серверу баз данных PostgreSQL
description: Подключение сервера приложений Jboss 7.X к серверу баз данных PostgreSQL
keywords: JBoss 7.1, PostgreSQL
permalink: /appservers/jboss/7.1/jboss-jdbc-connection-to-posgresql/
---

# Подключение сервера приложений Jboss 7.X к серверу баз данных PostgreSQL

**Качаю:** с сайта https://jdbc.postgresql.org/download.html
Драйвер postgresql-9.4-1201.jdbc41.jar

    $ mkdir -p /opt/jboss/7.1.1/modules/org/postgresql/main
    $ cd /opt/jboss/7.1.1/modules/org/postgresql/main
    $ wget  https://jdbc.postgresql.org/download/postgresql-9.4-1201.jdbc41.jar

**jboss**

<br/>

### Подключение JDBC драйвера как модуля Jboss

    $ vi /opt/jboss/7.1.1/modules/org/postgresql/main/module.xml

// Чтобы текст в vi не уехал вправо

    :set paste

{% highlight xml %}

<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.0" name="org.postgresql">
    <resources>
        <resource-root path="postgresql-9.4-1201.jdbc41.jar"/>
    </resources>

    <dependencies>
        <module name="javax.api"/>
        <module name="javax.transaction.api"/>
    </dependencies>

</module>

{% endhighlight %}

// Отменяю ранее введенный параметр.
// Далее не буду умоминать его вызов.

    :set nopaste

<br/>

### PostgreSQL Datasource

// Делаю резервную копию файла standalone.xml

    $ cp /opt/jboss/7.1.1/standalone/configuration/standalone.xml /opt/jboss/7.1.1/standalone/configuration/standalone.xml.orig

<br/>

    $ vi /opt/jboss/7.1.1/standalone/configuration/standalone.xml

Вроде можно вынести описание datasourses в отдельный файл заканчивающийся на <mydatabase>-ds.xml

Заменяю datasources на вот это:

{% highlight xml %}

    <datasources>
             <datasource jta="false" jndi-name="java:jboss/postgresDS" pool-name="PostgresDS" enabled="true" use-ccm="false">
                 <connection-url>jdbc:postgresql://192.168.56.3:5432/mydatabase</connection-url>
                 <driver-class>org.postgresql.Driver</driver-class>
                 <driver>postgresql</driver>
                 <pool>
                    <min-pool-size>5</min-pool-size>
                    <max-pool-size>200</max-pool-size>
                </pool>
                 <security>
                     <user-name>scott</user-name>
                     <password>tiger</password>
                 </security>
                 <validation>
                     <validate-on-match>false</validate-on-match>
                     <background-validation>false</background-validation>
                     <background-validation-millis>1</background-validation-millis>
                 </validation>
                 <statement>
                     <prepared-statement-cache-size>0</prepared-statement-cache-size>
                     <share-prepared-statements>false</share-prepared-statements>
                 </statement>
             </datasource>

                   <drivers>
                       <driver name="postgresql" module="org.postgresql">
                          <xa-datasource-class>org.postgresql.Driver</xa-datasource-class>
                       </driver>
                   </drivers>
    </datasources>

{% endhighlight %}

<br/>

### PostgreSQL XA Datasource

\$ vi /opt/jboss/7.1.1/standalone/configuration/standalone.xml

{% highlight xml %}

<datasources>
***

<xa-datasource jndi-name="java:jboss/PostgresXADS" pool-name="PostgresXADS" enabled="true" use-ccm="false">
      <xa-datasource-property name="ServerName">
          192.168.56.3
      </xa-datasource-property>
      <xa-datasource-property name="PortNumber">
          5432
      </xa-datasource-property>
      <xa-datasource-property name="DatabaseName">
          mydatabase
      </xa-datasource-property>
      <driver>postgresqlXA</driver>
      <xa-pool>
          <min-pool-size>5</min-pool-size>
          <max-pool-size>200</max-pool-size>
          <is-same-rm-override>false</is-same-rm-override>
          <interleaving>false</interleaving>
          <pad-xid>false</pad-xid>
          <wrap-xa-resource>false</wrap-xa-resource>
      </xa-pool>
      <security>
          <user-name>scott</user-name>
          <password>tiger</password>
      </security>
      <validation>
          <validate-on-match>false</validate-on-match>
          <background-validation>false</background-validation>
      </validation>
      <statement>
          <share-prepared-statements>false</share-prepared-statements>
      </statement>
  </xa-datasource>

---

  <drivers>

    ***
      <driver name="postgresqlXA" module="org.postgresql">
         <xa-datasource-class>org.postgresql.xa.PGXADataSource</xa-datasource-class>
      </driver>
    ***

  </drivers>

</datasources>

{% endhighlight %}

---

Ошибка:

    Caused by: org.postgresql.util.PSQLException: FATAL: Ident authentication failed for user "scott" ...

Была по причине того, что неправильно были настроены правила в конфиге postgresql сервера.  
/var/lib/pgsql/data/pg_hba.conf

На тестовом окружении, я просто тупо поменял записи вида ident на trust. Все заработало.

local all all peer

---

https://access.redhat.com/documentation/en-US/JBoss_Enterprise_Application_Platform/6/html/Administration_and_Configuration_Guide/Example_PostgreSQL_XA_Datasource.html
