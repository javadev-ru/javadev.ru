---
layout: page
title: HTTPS
description: HTTPS
keywords: HTTPS
permalink: /https/
---

# HTTPS

<a href="/https/ssl-basics/">Основы SSL</a><br/>
<a href="/https/ssl-keystore-java/">Keystore в Java, keytool</a><br/>

### Встроенный в JDK сервер для тестирования

<a href="/https/using-jdk-built-in-http-server/">Настройка встроенного в JDK HTTP сервера для работы по HTTP</a><br/>

<a href="/https/configure-embedded-jdk-https-server/">Настройка встроенного в JDK HTTP сервера для работы по HTTPS</a><br/>

<a href="/https/configure-embedded-jdk-https-server-without-check/">Пример простого HTTPS соединения без проверки сертификата сервера</a><br/>

<a href="/https/configure-embedded-jdk-https-server-with-check/">Пример клиентского HTTPS соединения с проверкой серверного сертификата</a><br/>

<a href="/https/configure-embedded-jdk-http-server-for-https-2-factor-auth/">Пример HTTPS клиента и HTTPS сервера с двусторонней аутентификацией</a><br/>

### TomCat

<a href="/https/configure-https-at-apache-tomcat-and-https-test-client/">Настройка HTTPS на Tomcat и тестовый HTTPS клиент</a>

Исходники теста можно забрать с github:<br/> https://github.com/chesnokov/dev64-samples/tree/master/ssl-test
