---
layout: page
title: Keystore в Java
description: Keystore в Java
keywords: Keystore в Java
permalink: /https/ssl-keystore-java/
---

# Keystore в Java

### Что такое Java Keystore?

Keystore используется для хранения собственных приватных ключей и сертификатов сервера или клиента.

Для аутентификации клиента и сервера устанавливающих SSL соединение требуются приватные ключи и сертификаты. Если используется односторонняя аутентификация, то keystore нужен только на серверной стороне. При двусторонней аутентификации и клиент и сервер обмениваются сертификатами, соответственно и у сервера, и у клиента должен быть keystore с парой приватный ключ/публичный ключ + сертификат.

Т.е. иными словами Keystore используется для хранения ключей и сертификатов, использующихся для идентификации владельца ключа (клиента или сервера).

### Trust Store

Второй тип keystore применяется для хранения trusted сертификатов. В него кладутся ключи trusted certificate authorities CA. При пользовании самоподписанными сертификатами, в trusted store может класться самоподписанный сертификат. Это тоже keystore, но в Java он называется trusted store.

Форматы Keystore поддерживаемые Java

Т.о., как описано выше, keystore — контейнер, используемый для хранения ключей и сертификатов. Java поддерживает два формата keystore:

- JKS (Java Key Store) – Java format
- PKCS12 — this is an industry standard

Тип keystore, используемый по-умолчанию, задается в Java security properties файле свойством keystore.type. Если приложение обращается к key store файлу без явного указания его типа, используется JKS формат. Java security properties файл расположен в каталоге lib внутри инсталляционного директория с Java по пути: /lib/security/java.security

Для работы с keystore в java дистрибутиве есть специальная утилита keytool. Keytool вполне достаточно для операций с ключами в Java. Однако JKS формат является пропиетарным и закрытым. Поэтому часто для разнообразных конвертаций и взаимодействия со сторонними разработчиками могут использоваться утилиты, поставляемые в комплекте с библиотекой OpenSSL.

В тех случаях, когда планируется использовать ключи исключительно в Java keystore в формате JKS вполне подойдет.

### Алиасы

Keystore (по крайней мере в JKS формате), позволяет хранить несколько пар ключей и сертификатов. Для идентификации каждой пары или отдельного сертификата используется алиас. Алиас указывается в исходном коде при доступе к соответствующему ключу или сертификату. Доступ к каждому алиасу ограничивается паролем.

### Создание keystore

Процесс генерации keystore (JKS или PKS12) включает генерацию пары ключей (приватного и публичного). Затем получение от Certificate Authority (CA) подписи к публичному ключу и связанной с ним идентифицирующей информации в виде сертификата. Certificate authority генерирует сертификат на основе публичного ключа и идентификационной информации, переданной ему в виде CSR.

Wikipedia говорит, что CA выдает сертификат, привязывающий публичный ключ к указанному Distinguished Name (это может быть имя хоста (hostname, имя пользователя или название приложения). Шаги по созданию keystore представляющего пользователя, приложение или хост следующие:

1. Сгенерировать пару ключей (public / private key)

В java при генерации пары ключей с помощью keytool сразу создается самоподписанный self-signed сертификат, который можно немедленно использовать для тестирования. Следующие шаги, таким образом, нужны только для создания полноценного официального сертификата.

2. Сгенерировать запрос на получение сертификата (Certificate Signing Request (CSR)).

3. Получить CSR, подписанный доверенным CA (output of this is a certificate)

4. Импортировать сертификат, сделанный CA в ваш key store.

5. Импортировать сертификат CA в ваш truststore как trusted certificate

### Java’s Default Keystore

Веб-сервер или приложение может сказать Java использовать заданный keystore файл установкой свойства javax.net.ssl.keyStore. (указывается путь к файлу keystore). Если приложение не укажет keystore property, тогда загружается keystore по-умолчанию. Keystore по-умолчанию хранится в файле .keystore в пользовательском домашнем директории, определяемом в свою очередь системным свойством user.home.

### Java’s Default Truststore

Приложение может указать Java использовать определенный truststore файл установкой свойства javax.net.ssl.trustStore. Если приложение не указывает явно truststore, тогда truststore по-умолчанию загружается и используется.
По-умолчанию java truststore находится в /lib/security/cacerts и его пароль по-умолчанию: ‘changeit’. Файлы truststore — обычные keystore файлы, содержащие один или более сертификатов trusted CA (Certificate Authorities).

### Keytool

Для облегчения создания и управления keystore файлами в дистрибутив Java входит утилита keytool, позволяющая создавать JKS файлы. Keytool позволяет управление сертификатами и парами публичных приватных ключей.

С опцией -genkey, keytool генерирует новую пару public/private ключей, и для public ключа создает self-signed сертификат.

    keytool -genkey -keystore server.jks -dname "CN=localhost, OU=dev64, O=dev64-wordpress, L=Unknown, ST=Unknown, C=RU" -storepass storepass -alias server-test -keypass serverpass

Сертификат создается в формате X.509. В этом формате в качестве идентификатора владельца используется Distinquished Name или просто DN в формате X.500. Точно такой же формат идентификации объектов используется, например в LDAP-протоколе или в SNMP. Distinquished Name задается в виде разделенных через запятую атрибутов: «CN=Andrey Chesnokov, OU=dev64, O=dev64-wordpress, L=Unknown, ST=Unknown, C=RU». Здесь отдельные атрибуты расшифровываются так:

- CN — common name имя владельца
- OU — organizationUnit (e.g, department or division) департамент или отдел
- O — organizationName — large organization name, e.g., «ABCSystems, Inc.»
- L — localityName — locality (city) name, e.g., «Palo Alto» местоположение (город)
- ST — stateName — state or province name, e.g., «California»
- C — country — two-letter country code, e.g., «CH»

Часть из атрибутов могут быть пропущены, в данном случае им присвоено значение Unknown. При генерации тестового keystore, значения можно присваивать любые. При получении официального сертификата, данные регламентируются и проверяются Certificate Authority организацией.

Внутри каждого сертификата в формате X.509 хранится пара Distinqueshed Names (DN), один DN принадлежит владельцу сертификата, а второй DN указывает идентификатор CA, подписавшей сертификат. В случае с self-signed сертификатом, оба эти DN указывают на владельца сертификата.

Distinquished Name задается keytool с помощью опции -dname. Если опцию dname не указать, тогда keytool запросит все необходимые поля из командной строки.

Опция keystore задает имя keystore файла. Если её пропустить, тогда keytool создаст файл с именем .keystore в домашнем директории пользователя.

Доступ к keystore защищается паролем. Соответственно опция -keypass указывает пароль для доступа к keystore целиком. Этот пароль необходим для возможности чтения или модификации keystore.

Второй пароль, как уже говорилось выше, необходим для доступа к отдельному алиасу внутри keystore. Этот второй пароль указывается с помощью опции -keypass.

Т.о. с помощью одной команды получается keystore с парой ключей и сертификатом, остается этот keystore взять и подложить в нужный каталог к своей программе. Но это уже другая история.

Обязательно стоит почитать официальную документацию на keytool, она содержит много полезного.

    Keytool Oracle documentation (http://docs.oracle.com/javase/1.4.2/docs/tooldocs/windows/keytool.html)

Взято:  
https://dev64.wordpress.com/2013/06/17/ssl-keystore-java/

Некоторые команды для работы с сертификатами:

    keytool -list -keystore server.jks

    Enter keystore password:

    Keystore type: JKS
    Keystore provider: SUN

    Your keystore contains 1 entry

    server-test, Apr 9, 2015, PrivateKeyEntry,
    Certificate fingerprint (SHA1): E3:3F:3B:D9:3B:4E:34:8E:D2:D5:D5:B6:9E:C2:86:57:DE:4D:A8:A9

<br/>

    keytool -export -keystore server.jks -alias server-test -storepass storepass -file server.cer
    Certificate stored in file <server.cer>


    keytool -import -keystore clienttrust.jks -file server.cer -storepass storepass
        Owner: CN=localhost, OU=dev64, O=dev64-wordpress, L=Unknown, ST=Unknown, C=RU
    Issuer: CN=localhost, OU=dev64, O=dev64-wordpress, L=Unknown, ST=Unknown, C=RU
    Serial number: 16797d2
    Valid from: Thu Apr 09 11:54:14 UTC 2015 until: Wed Jul 08 11:54:14 UTC 2015
    Certificate fingerprints:
    	 MD5:  72:A0:86:C9:DE:9A:E4:0C:99:B0:F9:80:F8:C4:D3:A9
    	 SHA1: E3:3F:3B:D9:3B:4E:34:8E:D2:D5:D5:B6:9E:C2:86:57:DE:4D:A8:A9
    	 SHA256: CC:66:89:7E:76:F4:C8:35:26:D7:E3:88:27:33:96:19:22:59:7D:63:13:D5:14:11:87:C6:80:4E:05:7B:CB:DA
    	 Signature algorithm name: SHA1withDSA
    	 Version: 3

    Extensions:

    #1: ObjectId: 2.5.29.14 Criticality=false
    SubjectKeyIdentifier [
    KeyIdentifier [
    0000: E2 20 A1 7F 0C 55 A4 18   43 25 73 17 CE E4 E8 72  . ...U..C%s....r
    0010: 93 73 F4 00                                        .s..
    ]
    ]

    Trust this certificate? [no]:  y
    Certificate was added to keystore
