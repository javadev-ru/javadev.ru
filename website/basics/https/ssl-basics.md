---
layout: page
title: Основы SSL
description: Основы SSL
keywords: Основы SSL
permalink: /https/ssl-basics/
---

# Основы SSL

SSL — семейство протоколов для установки шифрованного соединения между двумя сторонами, желающими обмениваться данными. Это обмен в процессе которого соединяющиеся стороны договариваются какие механизмы шифрования каждый из них поддерживает, какую длину ключа использовать, режимы шифрования, производится обмен сертификатами, несимметричный алгоритм шифрования используется для обмена ключом симметричного протокола шифрования, который потом используется при дальнейшем взаимодействии. Такой начальный обмен данными называется handshake.

Симметричный ключ генерируется во время handshake и действует только для одной SSL сессии. Если сессия не остается активной, ключ устаревает (expire). Максимальное время после которого SSL сессия устаревает (expire) может быть задано. После устаревания сессии handshake должен быть повторен с начала. Результатом установки сессии является новый симметричный ключ.

### Сертификаты и ключи

Прежде чем углубляться в детали SSL, нужно ввести понятие ключей и сертификатов. SSL/TLS используют асимметричное шифрование для аутентификации и для обмена симметричным ключом, который будет использоваться для шифрования данных во время сессии.

Асимметричное шифрование использует пары ключей для шифрования — приватный ключ и соответствующий ему публичный ключ. Данные зашифрованные одним ключом могут быть расшифрованы парным ключом. Аналогично данные подписанные одним ключом, могут быть проверены вторым ключом. Приватный ключ, как предполагает его имя, должен храниться в секрете.

### Сертификаты.

Сертификат состоит из публичного ключа, вместе с некоторой идентифицирующей владельца информацией и даты прекращения срока действия ключа. Также сертификат содержит цифровую подпись организации выдавшей сертификат (certificate authority CA). Цифровая подпись гарантирует, что сертификат не подделан. Сертификат обычно выдается веб-серверу, приложению или пользователю. Сертификат является способом распространения публичного ключа шифрования.

Если клиент шифрует сообщение публичным ключом сервера (из сертификата сервера), клиент может быть уверен, что только легальный сервер сможет расшифровать сообщение. В процессе установления SSL/TLS сессии, клиент создает часть сессионного ключа, шифрует сообщение с помощью публичного ключа сервера и передает на сервер. Если сервер тот, за кого себя выдает, он сможет расшифровать сообщение с помощью приватного ключа и достать из сообщения сессионный ключ.

Сертификаты бывают двух типов.

- Официально выданные сертификаты, подписанные Certificate Authority организацией
- Self-signed сертификаты

Self-signed сертификаты — сертификаты введенные для тестирования, чтобы разработчики, не дожидаясь получения официально-подписанного сертификата могли тестировать свое программное обеспечение. Self-signed сертификат отличается тем, что подлинность его невозможно проверить, если только вы его не сделали лично или не получили на цифровом носителе из надежного источника. В остальном self-signed сертификаты точно такие, как и официальные. Программно они могут использоваться точно также.

### Доверие (Trust)

Ключевым понятием SSL соединения является концепция доверия (trust) сертификату. Важным является способ получения сертификата, используемого для соединения. Если вы получаете сертификат из надежного источника, например лично от владельца сайта, вы можете быть уверенным что сертификат подлинный и вы действительно соединяетесь с настоящим сайтом. Однако при установке соединения из веб-браузера, к примеру, сертификат сервера может получаться с самого сервера, с которым вы устанавливаете соединение. Встает вопрос подлинности сертификата. Что если хакер создал его собственную пару асимметричных ключей и затем сделал собственный сертификат для подделки сервера банка?

Модель доверия, простая. Каждый клиент или сервер решает, что он доверяет определенным организациям (certificate authorities (CA)) выдающим сертификаты. Доверять CA, значит доверять что любые сертификаты выданные CA легальные и что идентифицирующая информация в сертификате корректна и заслуживает доверия. Verisign — пример CA, подписывающий множество сертификатов для больших Internet компаний. Все браузеры верят Verisign по умолчанию. Идентификационная информация сертификата содержит цифровую подпись, сгенерированную CA. Клиент или сервер доверяет CA, добавляя сертификат в файл хранилище, называемый ‘truststore’. Сохранение CA сертификата в truststore позволяет java проверять цифровую подпись сертификата сгенерированную CA и решить доверять сертификату или нет.

Если хакер подкладывает поддельный сертификат для Barclay’s банка, ваш браузер будет пытаться проверить цифровую подпись у сертификата. Эта проверка будет не успешной поскольку в truststore нет сертификата, которым подписан сертификат злоумышленника.

### Certificate Chain

На практике формат сертификатов таков, что в сертификат может входить множество подписей. В сертификате хранится не одна подпись а некий «Certificate Chain»

Когда генерируются приватный и публичные ключи, для публичного ключа автоматически генерируется self-signed сертификат. Т.е. изначально вы получаете пару из приватного ключа и сертификата, содержащего подписанный публичный ключ. Self-signed certificate — такой сертификат, в котором создатель ключа и подписывающий является одним лицом.

Позже, после генерации Certificate Signing Request (CSR) и получения ответа от Certification Authority (CA), self-signed (самоподписанный) сертификат заменяется на цепочку сертификатов (Сertificate Chain). В цепочку добавляется сертификат от CA, подтверждающий подлинность публичного ключа, за ним сертификат, подтверждающий подлинность публичного ключа CA.

Во многих случаях, последний сертификат в цепочке (удостоверяющий подлинность публичного ключа CA) сам является самоподписанным. В других случаях, CA в свою очередь, может вернуть не один а цепочку сертификатов. В таком случае, первым в цепочке лежит публичный ключ, подписанный CA который выдал сертификат, а вторым сертификатом может быть сертификат другого CA, подтверждающий подлинность публичного ключа первого CA, которому посылался запрос на подпись. Следом будет идти сертификат, подтверждающий подлинность публичного ключа второго CA, и т.д. до тех пор пока цепочка не закончится самоподписанным корневым root — сертификатом. Каждый сертификат в цепочке подтверждает подлинность публичного ключа предыдущего сертификата в цепочке.

Рассмотрим SSL глубже. Бывает два вида SSL соединений.

### Простая аутентификация (1 way SSL authentication)

Перед установкой шифрованного SSL/TLS соединения должна быть выполнена аутентификация. Чтобы простое соединение могло быть установлено, должен быть установлен сервер с закрытым ключом, для которого получен сертификат. Клиент также должен хранить список организаций СА, которым он доверяет.

imx_onewayssl.gif

Клиент проверяет подлинность сервера перед установкой шифрованного соединения.

### 2 way SSL authentication (двусторонняя аутентификация)

При таком типе аутентификации и сервер, и клиент, оба предоставляют сертификаты для проверки подлинности друг-друга перед установкой шифрованного соединения.

imx_twowaysslcacert.png

### Проверка сертификата

При односторонней аутентификации (первый тип), сервер имеет сертификат с парным приватным ключом. Данные зашифрованные с помощью публичного серверного ключа могут быть расшифрованы с помощью приватного ключа на сервере. Клиент, желающий установить соединение с сервером, проверяет сертификат сервера прежде чем переходить к установке шифрованного соединения. Клиент должен проверить следующее:

- Действителен ли по-прежнему сертификат (не завершился ли срок действия сертификата (expiry date passed))
- Сертификат предоставленный сервером действительно соответствует его имени хоста.
- Есть ли организация выдавшая сертификат CA в списке, которому клиент доверяет?
- Проверить цифровую подпись на сертификате

Проверяются также другие мелкие детали, такие как алгоритм шифрования цифровой подписи, длины ключа и т.д.

При 2 стороней аутентификации клиент и сервер оба владеют приватными ключами и сертификатами. Оба проверяют сертификаты друг-друга, перед установкой шифрованного соединения. Клиент проделывает проверки описанные выше и сервер выполняет те же действия над клиентским сертификатом.

### Запрос подписи и подписывание сертификатов

Создание ключей и сертификатов регламентируется стандартами.

Ключи генерируются в соответствии с PKCS (http://ru.wikipedia.org/wiki/PKCS).

Когда пара состоящая из публичного и приватного ключей сгенерирована, создается объект запроса на сертификат, называющийся Certificate Signing Request (CSR), регламентируемый стандартом PKCS#10. Trusted CA (certificate authority) должен затем решить хочет ли он подписать CSR, доверяет ли он запрашивающему регистрацию клиенту и предоставленной им информации в его сертификате.

Если CA (certificate authority) рещает доверять запросу на подпись сертификата (CSR), результатом становится выпуск подисанного сертификата с идентификационной информацией предоставленной в CSR. Сертификат регламентируется стандартом X.509.

Взято:
https://dev64.wordpress.com/2013/06/12/ssl-basic/
