---
layout: page
title: Материалы, которые могут быть полезны изучающим английский язык
description: Материалы, которые могут быть полезны изучающим английский язык
keywords: Материалы, которые могут быть полезны изучающим английский язык
permalink: /english/
---

# Материалы, которые могут быть полезны изучающим английский язык

Кому интересно, сейчас я пытаюсь учить английский следующими способами:

    1) Начинать, как мне видится, нужно с неправильных глаголов (English irregular verbs). Сразу учить 3 формы.

    Была отличная раздача на трекере. Но ее потерли. Хорошо, что у меня осталась.

    2) Аудио подкасты на английском. https://rutracker.org/forum/viewtopic.php?t=4885469 и https://rutracker.org/forum/viewtopic.php?t=4855762 (Слушаю в плеере по дороге на работу и обратно)

    3) Бесплатная программа для запоминания английских слов - Анки.

    https://rutracker.org/forum/viewtopic.php?t=4716687
    https://rutracker.org/forum/viewtopic.php?t=4811029

    (Кликаю пальцами, когда в метро есть такая возможность). Я использую ее на Android, но она работает и на других платформах. До меня не сразу дошло, что для импорта словарей, нужно использовать какой-нибудь файловый менеджер, который видит корневой раздел файловой системы.

    4) https://www.duolingo.com/ , lingualeo.com, lingvist.com (будут клянчить $) Сайты на котором можно изучать и английский.

    5) Английский грамматика - видео на английском:
    https://rutracker.org/forum/viewtopic.php?t=5279164

    Вот этот материал очень нравится:
    https://www.youtube.com/playlist?list=PL6BDo90oiwpS4_AM1c0s0ozpROeE2A9ff

    6) Я пытаюсь писать на английском. И с индусами мы уже находим общий язык. Если кто готов помогать или хотя бы исправлять ошибки на javadev.ru / javadev.org. Могу с теми делиться поступающей информацией или еще чего.

<br/>

Считаю, что русскоговорящие не смогут научить правильному произношению и не стоит на них даже тратить особого времени. Поэтому всякие курсы типа "Шпионский английский" - не для меня.

<br/>

Буду признателен за толковые советы по изучению английского. Особенно интересует грамматика.

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/eU-eC7kyaXY" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

А пока то, что взято у поляка.

<br/>

https://github.com/Vedenin/code-for-learning-languages

It's may be easy way to learn grammar for programmer then traditional method.

#### Positive sentence

| Tense          | Simple                                               | Continuous                                                                       | Perfect                                                      | Perfect Continuous                                                   |
| -------------- | ---------------------------------------------------- | -------------------------------------------------------------------------------- | ------------------------------------------------------------ | -------------------------------------------------------------------- |
| Past           | `pronoun + verb + "ed"`                              | `pronoun + (pronoun == I || pronoun == HE_SHE_IT? WAS: WERE) + verb + "ing"`     | `pronoun + HAD + verb + "ed"`                                | `pronoun + HAD + BEEN + verb + "ing"`                                |
| Present        | `pronoun + verb + (pronoun == HE_SHE_IT ? "s" : "")` | `pronoun + (pronoun == I ? AM : pronoun == HE_SHE_IT ? IS : ARE) + verb + "ing"` | `pronoun + (pronoun == HE_SHE_IT ? HAS: HAVE) + verb + "ed"` | `pronoun + (pronoun == HE_SHE_IT ? HAS: HAVE) + BEEN + verb + "ing"` |
| Future         | `pronoun + WILL + verb`                              | `pronoun + WILL + BE + verb + "ing"`                                             | `pronoun + WILL + HAVE + verb + "ed"`                        | `pronoun + WILL + HAVE + BEEN + verb + "ing"`                        |
| Future In Past | `pronoun + WOULD + BE + verb + "ing"`                | `pronoun + WOULD + BE + verb + "ing"`                                            | `pronoun + WOULD + HAVE + verb + "ed"`                       | `pronoun + WOULD + HAVE + BEEN + verb + "ing"`                       |

#### Negative sentence

| Tense   | Simple                                                         | Continuous                                                                                | Perfect                                                               | Perfect Continuous                                                            |
| ------- | -------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | --------------------------------------------------------------------- | ----------------------------------------------------------------------------- |
| Past    | `pronoun + DID +"not " + verb`                                 | `pronoun + (pronoun == I || pronoun == HE_SHE_IT? WAS: WERE) + "not " + verb + "ing"`     | `pronoun + HAD + "not " + verb + "ed"`                                | `pronoun + HAD + "not " + BEEN + verb + "ing"`                                |
| Present | `pronoun + (pronoun == HE_SHE_IT ? DOES : DO) + "not " + verb` | `pronoun + (pronoun == I ? AM : pronoun == HE_SHE_IT ? IS : ARE) + "not " + verb + "ing"` | `pronoun + (pronoun == HE_SHE_IT ? HAS: HAVE) + "not " + verb + "ed"` | `pronoun + (pronoun == HE_SHE_IT ? HAS: HAVE) + "not " + BEEN + verb + "ing"` |
| Future  | `pronoun + WILL + "not " + verb`                               | `pronoun + WILL + "not " + BE + verb + "ing"`                                             | `pronoun + WILL + "not " + HAVE + verb + "ed"`                        | `pronoun + WILL + "not " + HAVE + BEEN + verb + "ing"`                        |

#### Questions

| Tense   | Simple                                                      | Continuous                                                                        | Perfect                                                       | Perfect Continuous                                                    |
| ------- | ----------------------------------------------------------- | --------------------------------------------------------------------------------- | ------------------------------------------------------------- | --------------------------------------------------------------------- |
| Past    | `DID + pronoun + verb + "?"`                                | `(pronoun == I || pronoun == HE_SHE_IT? WAS: WERE) + pronoun + verb + "ing?"`     | `HAD + pronoun + verb + "ed?"`                                | `HAD + pronoun + BEEN + verb + "ing?"`                                |
| Present | `(pronoun == HE_SHE_IT ? DOES : DO) + pronoun + verb + "?"` | `(pronoun == I ? AM : pronoun == HE_SHE_IT ? IS : ARE) + pronoun + verb + "ing?"` | `(pronoun == HE_SHE_IT ? HAS: HAVE) + pronoun + verb + "ed?"` | `(pronoun == HE_SHE_IT ? HAS: HAVE) + pronoun + BEEN + verb + "ing?"` |
| Future  | `WILL + pronoun + verb + "?"`                               | `WILL + pronoun + BE + verb + "ing?"`                                             | `WILL + pronoun + HAVE + verb + "ed?"`                        | `WILL + pronoun + HAVE + BEEN + verb + "ing?"`                        |

#### Passive

| Tense   | Simple                                                                          | Continuous                                                                              | Perfect                                                             |
| ------- | ------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- | ------------------------------------------------------------------- |
| Past    | `pronoun + (pronoun == I || pronoun == HE_SHE_IT? WAS: WERE) + verb + "ed"`     | `pronoun + (pronoun == I || pronoun == HE_SHE_IT? WAS: WERE) + BEING + verb + "ed"`     | `pronoun + HAD + BEEN + verb + "ed"`                                |
| Present | `pronoun + (pronoun == I ? AM : pronoun == HE_SHE_IT ? IS : ARE) + verb + "ed"` | `pronoun + (pronoun == I ? AM : pronoun == HE_SHE_IT ? IS : ARE) + BEING + verb + "ed"` | `pronoun + (pronoun == HE_SHE_IT ? HAS: HAVE) + BEEN + verb + "ed"` |
| Future  | `pronoun + WILL + BE + verb + "ed"`                                             | NULL                                                                                    | `pronoun + WILL + HAVE + BEEN + verb + "ed"`                        |
