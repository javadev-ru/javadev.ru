---
layout: page
title: Site Map
description: Site Map
keywords: Site Map
permalink: /sitemap/
---

# Site Map

<strong><a href="/appservers/jboss/7.1/installation/">Инсталляция сервера приложений JBoss 7.1 в операционной системе Centos 6.6 x86 64 bit</a></strong>

<strong><a href="/appservers/jboss/7.1/jboss-jdbc-connection-to-posgresql/">Подключение сервера приложений Jboss 7.X к серверу баз данных PostgreSQL</a></strong>

<strong><a href="/appservers/weblogic/12c/installation/">Инсталляция сервера приложений Oracle Weblogic 12c в операционной системе Oracle Linux 5.8 x86 64 bit</a></strong>

<hr/>

### https, ssl, сертификаты в java

<a href="https://javadev.ru/https/">Пытался разобраться здесь</a>
